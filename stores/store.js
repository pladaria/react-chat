var Store = function(items) {
    this.items = items || [];
};

Store.prototype = new EventEmitter();

Store.prototype.add = function(item) {
    this.items.push(item);
    this.emit('change');
};

Store.prototype.getAll = function() {
    return this.items;
};

Store.prototype.count = function() {
    return this.items.length;
}

Store.prototype.remove = function(index) {
    this.items.splice(index, 1);
    this.emit('change');
}
